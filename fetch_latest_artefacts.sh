#!/usr/bin/env bash

set -o errtrace
set -o nounset

artefact_dir="./artefacts"
sbom_artefact_dir="${artefact_dir}/sbom-tool"
bomber_artefact_dir="${artefact_dir}/bomber"

echo "Cleaning old artefacts..."
rm -rf "$artefact_dir"
mkdir -p "$sbom_artefact_dir"
mkdir -p "$bomber_artefact_dir"

latest_sbom_tool=$(curl -s https://api.github.com/repos/microsoft/sbom-tool/releases/latest)
sbom_tool_assets=$(jq -r '.assets[].browser_download_url' <<<"$latest_sbom_tool")

echo "Getting sbom-tool artefacts..."
for asset in $sbom_tool_assets; do
    # shellcheck disable=SC2001
    echo "  Considering $(sed 's|.*releases/download/.*/||g' <<<"$asset")"
    wget --quiet --directory-prefix "$sbom_artefact_dir" "$asset"
done

latest_bomber=$(curl -s https://api.github.com/repos/devops-kung-fu/bomber/releases/latest)
bomber_assets=$(jq -r '.assets[].browser_download_url' <<<"$latest_bomber")

echo "Getting bomber artefacts..."
for asset in $bomber_assets; do
    # shellcheck disable=SC2001
    echo "  Considering $(sed 's|.*releases/download/.*/||g' <<<"$asset")"
    wget --quiet --directory-prefix "$bomber_artefact_dir" "$asset"
done

echo "Done!"